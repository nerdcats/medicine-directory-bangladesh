{
  "Name": "X-cold",
  "Manufacturer": "Acme",
  "OrigName": "ambroxol hcl",
  "Details": "X-cold is a preparation of ambroxol hcl.\r\n\r\n  Ambroxol is a mucolytic agent or expectorant which dissolves thick mucus and is usually used to help relieve respiratory difficulties.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "COUGH AND COLD PREPARATIONS"
  ]
}
{
  "Name": "X-pectoran",
  "Manufacturer": "Rangs",
  "OrigName": "bromhexine",
  "Details": "X-pectoran is a preparation of bromhexine.\r\n\r\n  Bromhexine is a mucolytic agent used in the treatment of respiratory disorders associated with abnormal mucus secretion and impaired mucus transport. Bromhexine helps to transport the phlegm (cough) out of the lungs and is used in antitussive (cough) syrups.\n",
  "Dosage": "",
  "Available": [
    "X-pectoran 8 mg Tab. :Contains bromhexine hydrochloride 8 mg/tablet."
  ],
  "Classification": [
    "COUGH AND COLD PREPARATIONS"
  ]
}
{
  "Name": "Xalatan",
  "Manufacturer": "Pfizer",
  "OrigName": "latanoprost",
  "Details": "Xalatan is a preparation of latanoprost.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "xaliproden",
  "Manufacturer": null,
  "OrigName": null,
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "OTHER NERVOUS SYSTEM DRUGS"
  ]
}
{
  "Name": "Xamic",
  "Manufacturer": "Renata",
  "OrigName": "tranexamic acid",
  "Details": "Xamic is a preparation of tranexamic acid.\n",
  "Dosage": "",
  "Available": [
    "Xamic 500 mg Cap. :Contains tranexamic acid 500 mg/capsule."
  ],
  "Classification": [
    "ANTIHEMORRHAGICS"
  ]
}
{
  "Name": "xamoterol",
  "Manufacturer": null,
  "OrigName": null,
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "CARDIAC THERAPY"
  ]
}
{
  "Name": "Xanax",
  "Manufacturer": "Navana",
  "OrigName": "alprazolam",
  "Details": "Xanax is a preparation of alprazolam.\r\n\r\n  Alprazolam is used for the management of anxiety disorder or the short-term relief of symptoms of anxiety. Alprazolam is also used for the treatment of panic disorder, with our without agoraphobia.\n",
  "Dosage": "",
  "Available": [
    "Xanax 0.25 mg Tab. :Contains alprazolam 0.25 mg/tablet."
  ],
  "Classification": [
    "PSYCHOLEPTICS"
  ]
}
{
  "Name": "xantinol nicotinate",
  "Manufacturer": null,
  "OrigName": null,
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "PERIPHERAL VASODILATORS"
  ]
}
{
  "Name": "Xbac",
  "Manufacturer": "Beacon",
  "OrigName": "ciprofloxacin",
  "Details": "Xbac is a preparation of ciprofloxacin.\r\n\r\n  Preparations containing ciprofloxacin is indicated for the treatment of infections caused by susceptible strains of the designated microorganisms in the conditions and patient populations listed below:\n",
  "Dosage": "",
  "Available": [
    "Xbac 500 mg Tablet :Contains Ciprofloxacin 500 mg.",
    "Xbac 750 mg Tablet :Contains Ciprofloxacin 750 mg."
  ],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE",
    "OPHTHALMOLOGICALS",
    "OTOLOGICALS",
    "OPHTHALMOLOGICAL AND OTOLOGICAL PREPARATIONS"
  ]
}
{
  "Name": "Xcel",
  "Manufacturer": "ACI",
  "OrigName": "paracetamol/acetaminophen",
  "Details": "Xcel is a preparation of paracetamol/acetaminophen.\r\n\r\n  Paracetamol/acetaminophen is a pain reliever and fever reducer. It temporarily reduces fever\r\ntemporarily relieves minor aches and pains due to: headache, muscular aches, the common cold, minor pain of arthritis, backache, toothache, premenstrual and menstrual cramps.\n",
  "Dosage": "",
  "Available": [
    "Xcel 500 mg Tab.:Contains paracetamol/acetaminophen 500 mg/tablet."
  ],
  "Classification": [
    "ANALGESICS"
  ]
}
{
  "Name": "Xcel dt",
  "Manufacturer": "ACI",
  "OrigName": "paracetamol/acetaminophen",
  "Details": "Xcel dt is a preparation of paracetamol/acetaminophen.\r\n\r\n  Paracetamol/acetaminophen is a pain reliever and fever reducer. It temporarily reduces fever\r\ntemporarily relieves minor aches and pains due to: headache, muscular aches, the common cold, minor pain of arthritis, backache, toothache, premenstrual and menstrual cramps.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANALGESICS"
  ]
}
{
  "Name": "Xcel plus",
  "Manufacturer": "ACI",
  "OrigName": "paracetamol + caffeine",
  "Details": "Xcel plus is a preparation of paracetamol + caffeine.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANALGESICS"
  ]
}
{
  "Name": "Xcid",
  "Manufacturer": "Square",
  "OrigName": "calcium carbonate",
  "Details": "Xcid is a preparation of calcium carbonate.\r\n\r\n  Calcium carbonate is used as an antacid and as a calcium supplement in deficiency states and as an adjunct in the management of osteoporosis.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "DRUGS FOR ACID RELATED DISORDERS",
    "Mineral Supplements"
  ]
}
{
  "Name": "Xcitin",
  "Manufacturer": "Renata",
  "OrigName": "cyproheptadine hcl",
  "Details": "Xcitin is a preparation of cyproheptadine hcl.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIHISTAMINES FOR SYSTEMIC USE"
  ]
}
{
  "Name": "Xderm",
  "Manufacturer": "Bio-Pharma",
  "OrigName": "clobetasol propionate",
  "Details": "Xderm is a preparation of clobetasol propionate.\r\n\r\n  Clobetasol is a super-high potency corticosteroid. Preparations containing clobetasol are indicated for the relief of the inflammatory and pruritic manifestations of corticosteroid-responsive dermatoses. Treatment beyond 2 consecutive weeks is not recommended, and the total dosage should not exceed 50 g per week because of the potential for the drug to suppress the hypothalamic-pituitary-adrenal (HPA) axis. Use in pediatric patients under 12 years of age is not recommended.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "CORTICOSTEROIDS, DERMATOLOGICAL PREPARATIONS"
  ]
}
{
  "Name": "Xe fast",
  "Manufacturer": "Desh",
  "OrigName": "aceclofenac",
  "Details": "Xe fast is a preparation of aceclofenac.\r\n\r\n  Aceclofenac is a Nonsteroidal Anti-inflammatory Drug (NSAID) used in the management of osteoarthritis, rheumatoid arthritis, and ankylosing spondylitis.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS",
    "TOPICAL PRODUCTS FOR JOINT AND MUSCULAR PAIN"
  ]
}
{
  "Name": "Xegal",
  "Manufacturer": "Beximco",
  "OrigName": "gatifloxacin",
  "Details": "Xegal is a preparation of gatifloxacin.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "Xelcard",
  "Manufacturer": "Healthcare",
  "OrigName": "amlodipine besylate",
  "Details": "Xelcard is a preparation of amlodipine besylate.\r\n\r\n  Amlodipine is used for the treatment of hypertension (high blood pressure). It is also used in Coronary Artery Disease (CAD) such as Chronic Stable Angina, Vasospastic Angina.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "CALCIUM CHANNEL BLOCKERS"
  ]
}
{
  "Name": "Xelitor",
  "Manufacturer": "Novo Healthcare",
  "OrigName": "atorvastatin",
  "Details": "Xelitor is a preparation of atorvastatin.\r\n\r\n  Atorvastatin is used in :\n",
  "Dosage": "",
  "Available": [
    "Xelitor 10 mg Tab. :Contains atorvastatin 10 mg/tablet."
  ],
  "Classification": [
    "LIPID MODIFYING AGENTS"
  ]
}
{
  "Name": "Xeloda",
  "Manufacturer": "Roche",
  "OrigName": "capecitabine",
  "Details": "Xeloda is a preparation of capecitabine.\r\n\r\n  Capecitabine is a fluoropyrimidine with antineoplastic (anticancer) activity. It is used in following situations:\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTINEOPLASTIC AGENTS"
  ]
}
{
  "Name": "Xelopes",
  "Manufacturer": "Beacon",
  "OrigName": "omeprazole",
  "Details": "Xelopes is a preparation of omeprazole.\n",
  "Dosage": "",
  "Available": [
    "Xelopes 30 mg Capsule :Contains Omeprazole 20 mg."
  ],
  "Classification": [
    "DRUGS FOR ACID RELATED DISORDERS"
  ]
}
{
  "Name": "Xelpac",
  "Manufacturer": "Beacon",
  "OrigName": "paclitaxel",
  "Details": "Xelpac is a preparation of paclitaxel.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTINEOPLASTIC AGENTS"
  ]
}
{
  "Name": "Xelpid",
  "Manufacturer": "Healthcare",
  "OrigName": "atorvastatin",
  "Details": "Xelpid is a preparation of atorvastatin.\r\n\r\n  Atorvastatin is used in :\n",
  "Dosage": "",
  "Available": [
    "Xelpid 10 mg Tab. :Contains atorvastatin 10 mg/tablet."
  ],
  "Classification": [
    "LIPID MODIFYING AGENTS"
  ]
}
{
  "Name": "Xenapro",
  "Manufacturer": "Renata",
  "OrigName": "naproxen sodium",
  "Details": "Xenapro is a preparation of naproxen sodium.\n",
  "Dosage": "",
  "Available": [
    "Xenapro 250 mg Tab.:Contains naproxen sodium 250 mg/tablet."
  ],
  "Classification": [
    "OTHER GYNECOLOGICALS",
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS",
    "TOPICAL PRODUCTS FOR JOINT AND MUSCULAR PAIN"
  ]
}
{
  "Name": "Xenical",
  "Manufacturer": "Roche",
  "OrigName": "orlistat",
  "Details": "Xenical is a preparation of orlistat.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIOBESITY PREPARATIONS, EXCL. DIET PRODUCTS"
  ]
}
{
  "Name": "Xenim",
  "Manufacturer": "Opsonin",
  "OrigName": "cefepime hcl",
  "Details": "Xenim is a preparation of cefepime hcl.\r\n\r\n  Cefepime is a semi-synthetic, broad spectrum, cephalosporin antibiotic for parenteral administration. Cefepime for injection, USP is indicated in the treatment of the following infections caused by susceptible strains of the designated microorganisms:\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "Xenocard",
  "Manufacturer": "White Horse",
  "OrigName": "glyceryl trinitrate",
  "Details": "Xenocard is a preparation of glyceryl trinitrate.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "CARDIAC THERAPY",
    "VASOPROTECTIVES"
  ]
}
{
  "Name": "Xenocort",
  "Manufacturer": "Orion",
  "OrigName": "clobetasol propionate",
  "Details": "Xenocort is a preparation of clobetasol propionate.\r\n\r\n  Clobetasol is a super-high potency corticosteroid. Preparations containing clobetasol are indicated for the relief of the inflammatory and pruritic manifestations of corticosteroid-responsive dermatoses. Treatment beyond 2 consecutive weeks is not recommended, and the total dosage should not exceed 50 g per week because of the potential for the drug to suppress the hypothalamic-pituitary-adrenal (HPA) axis. Use in pediatric patients under 12 years of age is not recommended.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "CORTICOSTEROIDS, DERMATOLOGICAL PREPARATIONS"
  ]
}
{
  "Name": "Xenofer",
  "Manufacturer": "Beacon",
  "OrigName": "iron sucrose",
  "Details": "Xenofer is a preparation of iron sucrose.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIANEMIC PREPARATIONS"
  ]
}
{
  "Name": "xenon",
  "Manufacturer": null,
  "OrigName": null,
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANESTHETICS"
  ]
}
{
  "Name": "Xenoxin",
  "Manufacturer": "Eskayef",
  "OrigName": "levofloxacin",
  "Details": "Xenoxin is a preparation of levofloxacin.\n",
  "Dosage": "",
  "Available": [
    "Xenoxin 500 mg  Tab.:Contains levofloxacin 500 mg/tablet."
  ],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "Xenthol",
  "Manufacturer": "Eskayef",
  "OrigName": "methyl salicylate + menthol",
  "Details": "Xenthol is a preparation of methyl salicylate + menthol.\n",
  "Dosage": "",
  "Available": [],
  "Classification": []
}
{
  "Name": "xenysalate",
  "Manufacturer": null,
  "OrigName": null,
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "OTHER DERMATOLOGICAL PREPARATIONS"
  ]
}
{
  "Name": "Xepodox",
  "Manufacturer": "Rangs",
  "OrigName": "cefpodoxime",
  "Details": "Xepodox is a preparation of cefpodoxime.\r\n\r\n  Cefpodoxime is used in the treatment of infections due to susceptible organisms in following disease conditions :\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "Xerifen",
  "Manufacturer": "Peoples",
  "OrigName": "aceclofenac",
  "Details": "Xerifen is a preparation of aceclofenac.\r\n\r\n  Aceclofenac is a Nonsteroidal Anti-inflammatory Drug (NSAID) used in the management of osteoarthritis, rheumatoid arthritis, and ankylosing spondylitis.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS",
    "TOPICAL PRODUCTS FOR JOINT AND MUSCULAR PAIN"
  ]
}
{
  "Name": "Xeriflam",
  "Manufacturer": "Kumudini",
  "OrigName": "aceclofenac",
  "Details": "Xeriflam is a preparation of aceclofenac.\r\n\r\n  Aceclofenac is a Nonsteroidal Anti-inflammatory Drug (NSAID) used in the management of osteoarthritis, rheumatoid arthritis, and ankylosing spondylitis.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS",
    "TOPICAL PRODUCTS FOR JOINT AND MUSCULAR PAIN"
  ]
}
{
  "Name": "Xerod",
  "Manufacturer": "Beximco",
  "OrigName": "tegaserod",
  "Details": "Xerod is a preparation of tegaserod.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "DRUGS FOR FUNCTIONAL GASTROINTESTINAL DISORDERS"
  ]
}
{
  "Name": "Xerova",
  "Manufacturer": "Beacon",
  "OrigName": "atorvastatin",
  "Details": "Xerova is a preparation of atorvastatin.\r\n\r\n  Atorvastatin is used in :\n",
  "Dosage": "",
  "Available": [
    "Xerova 10 mg Tablet :Contains Atorvastatin 10 mg.",
    "Xerova 20 mg Tablet :Contains Atorvastatin 20 mg.",
    "Xerova 10 mg Tab. :Contains atorvastatin 10 mg/tablet."
  ],
  "Classification": [
    "LIPID MODIFYING AGENTS"
  ]
}
{
  "Name": "Xetril",
  "Manufacturer": "Beximco",
  "OrigName": "clonazepam",
  "Details": "Xetril is a preparation of clonazepam.\r\n\r\n  Clonazepam is a drug from benzodiazepine group. Preparations containing clonazepam is used to treat seizures and panic disorders.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIEPILEPTICS"
  ]
}
{
  "Name": "Xfin",
  "Manufacturer": "Square",
  "OrigName": "terbinafine  HCl",
  "Details": "Xfin is a preparation of terbinafine  HCl.\n",
  "Dosage": "",
  "Available": [
    "Xfin 1% cream:Each gram cream contains terbinafine hydrochloride INN equivalent to 10 mg terbinafine."
  ],
  "Classification": [
    "ANTIFUNGALS FOR DERMATOLOGICAL USE"
  ]
}
{
  "Name": "Xflam",
  "Manufacturer": "Square",
  "OrigName": "ibuprofen",
  "Details": "Xflam is a preparation of ibuprofen.\n",
  "Dosage": "",
  "Available": [
    "Xflam  200 Tablet:Each film-coated tablet contains Dexibuprofen INN 200 mg.",
    "Xflam  300 Tablet:Each film-coated tablet contains Dexibuprofen INN 300 mg."
  ],
  "Classification": [
    "CARDIAC THERAPY",
    "OTHER GYNECOLOGICALS",
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS",
    "TOPICAL PRODUCTS FOR JOINT AND MUSCULAR PAIN"
  ]
}
{
  "Name": "xibornol",
  "Manufacturer": null,
  "OrigName": null,
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "Xido",
  "Manufacturer": "Delta",
  "OrigName": "gliclazide",
  "Details": "Xido is a preparation of gliclazide.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "DRUGS USED IN DIABETES"
  ]
}
{
  "Name": "Xidolac",
  "Manufacturer": "Beximco",
  "OrigName": "ketorolac tromethamine",
  "Details": "Xidolac is a preparation of ketorolac tromethamine.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "Xidoloc",
  "Manufacturer": "Beximco",
  "OrigName": "ketorolac tromethamine",
  "Details": "Xidoloc is a preparation of ketorolac tromethamine.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "Xifim",
  "Manufacturer": "Bristol",
  "OrigName": "cefixime",
  "Details": "Xifim is a preparation of cefixime.\r\n\r\n  Cefixime is a third-generation cephalosporin antibacterial used to treat infections due to susceptible Gram-positive and Gram-negative bacteria, including gonorrhoea and infections of the respiratory and urinary tracts.\n",
  "Dosage": "",
  "Available": [
    "Xifim 200 mg Cap.:Contains cefixim 200 mg/capsule."
  ],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "Ximbac",
  "Manufacturer": "Proteety",
  "OrigName": "cefixime",
  "Details": "Ximbac is a preparation of cefixime.\r\n\r\n  Cefixime is a third-generation cephalosporin antibacterial used to treat infections due to susceptible Gram-positive and Gram-negative bacteria, including gonorrhoea and infections of the respiratory and urinary tracts.\n",
  "Dosage": "",
  "Available": [
    "Ximbac 200 mg Cap.:Contains cefixim 200 mg/capsule."
  ],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "ximelagatran",
  "Manufacturer": null,
  "OrigName": null,
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTITHROMBOTIC AGENTS"
  ]
}
{
  "Name": "Ximepime",
  "Manufacturer": "Globe",
  "OrigName": "cefepime hcl",
  "Details": "Ximepime is a preparation of cefepime hcl.\r\n\r\n  Cefepime is a semi-synthetic, broad spectrum, cephalosporin antibiotic for parenteral administration. Cefepime for injection, USP is indicated in the treatment of the following infections caused by susceptible strains of the designated microorganisms:\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "Ximeprox",
  "Manufacturer": "Incepta",
  "OrigName": "cefpodoxime",
  "Details": "Ximeprox is a preparation of cefpodoxime.\r\n\r\n  Cefpodoxime is used in the treatment of infections due to susceptible organisms in following disease conditions :\n",
  "Dosage": "",
  "Available": [
    "Ximeprox 100 mg Tablet :Contains cefpodoxime 100 mg.",
    "Ximeprox 200 mg Tablet :Contains cefpodoxime 200 mg.",
    "Ximeprox 100 mg Capsule :Contains Cefpodoxime 100 mg.",
    "Ximeprox Dry Suspension (100 ml):(\tContains cefpodoxime 40 mg/5 ml.",
    "Ximeprox DS Dry Suspension :Contains  cefpodoxime 80 mg/5 ml."
  ],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "Ximetil",
  "Manufacturer": "Globe",
  "OrigName": "cefuroxime",
  "Details": "Ximetil is a preparation of cefuroxime.\n",
  "Dosage": "",
  "Available": [
    "Ximetil 125 mg Tab:Contains cefuroximel 125 mg/tablet.",
    "Ximetil 250 mg Tab:Contains cefuroxime 250 mg/tablet.",
    "Ximetil 500 mg Tab:Contains cefuroxime 500 mg/tablet."
  ],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "Ximocef",
  "Manufacturer": "Kumudini",
  "OrigName": "cefpodoxime",
  "Details": "Ximocef is a preparation of cefpodoxime.\r\n\r\n  Cefpodoxime is used in the treatment of infections due to susceptible organisms in following disease conditions :\n",
  "Dosage": "",
  "Available": [
    "Ximocef 200 mg Cap.:Contains cefpodoxime 200 mg/capsule."
  ],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "Xinc",
  "Manufacturer": "Eskayef",
  "OrigName": "zinc sulfate",
  "Details": "Xinc is a preparation of zinc sulfate.\n",
  "Dosage": "",
  "Available": [
    "Xinc 20 Tab.:Contains elemental zinc 20 mg (as sulphate)/tablet",
    "Xinc 10 mg/5 ml Syp.:Contains elemental zinc 10 mg/5 ml (as sulphate)."
  ],
  "Classification": [
    "Mineral Supplements"
  ]
}
{
  "Name": "Xinc B",
  "Manufacturer": "Eskayef",
  "OrigName": "vitamin B-complex and zinc",
  "Details": "Xinc B is a preparation of vitamin B-complex and zinc.\n",
  "Dosage": "",
  "Available": [
    "Xinc B Syp.:Contains thiamine 5 mg + nicotinamide 20 mg + pyridoxine 2 mg + riboflavin 2 mg + elemental zinc 10 mg/ 5 ml."
  ],
  "Classification": [
    "Vitamins"
  ]
}
{
  "Name": "Xincs",
  "Manufacturer": "Eskayef",
  "OrigName": "zinc sulfate",
  "Details": "Xincs is a preparation of zinc sulfate.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "Mineral Supplements"
  ]
}
{
  "Name": "Xinoplex",
  "Manufacturer": "Silva",
  "OrigName": "vitamin B-complex and zinc",
  "Details": "Xinoplex is a preparation of vitamin B-complex and zinc.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "Vitamins"
  ]
}
{
  "Name": "Xinoplex I",
  "Manufacturer": "Silva",
  "OrigName": "iron + vitamin-b complex + zinc",
  "Details": "Xinoplex I is a preparation of iron + vitamin-b complex + zinc.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIANEMIC PREPARATIONS"
  ]
}
{
  "Name": "Xionil",
  "Manufacturer": "Novartis",
  "OrigName": "bromazepam",
  "Details": "Xionil is a preparation of bromazepam.\r\n\r\n  Bromazepam is a benzodiazepine that has been used in the short-term treatment of anxiety disorders occurring alone or associated with insomnia.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "PSYCHOLEPTICS"
  ]
}
{
  "Name": "xipamide",
  "Manufacturer": null,
  "OrigName": null,
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "DIURETICS"
  ]
}
{
  "Name": "Xirocip",
  "Manufacturer": "Novo Healthcare",
  "OrigName": "ciprofloxacin",
  "Details": "Xirocip is a preparation of ciprofloxacin.\r\n\r\n  Preparations containing ciprofloxacin is indicated for the treatment of infections caused by susceptible strains of the designated microorganisms in the conditions and patient populations listed below:\n",
  "Dosage": "",
  "Available": [
    "Xirocip 500 mg Tab.:Contains ciprofloxacin 500 mg/tablet."
  ],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE",
    "OPHTHALMOLOGICALS",
    "OTOLOGICALS",
    "OPHTHALMOLOGICAL AND OTOLOGICAL PREPARATIONS"
  ]
}
{
  "Name": "Xirom",
  "Manufacturer": "Aristopharma",
  "OrigName": "bromfenac",
  "Details": "Xirom is a preparation of bromfenac.\r\n\r\n  Bromfenac, a nonsteroidal anti-inflammatory drug (NSAID), is used for the treatment of postoperative inflammation and reduction of ocular pain after cataract extraction.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "Xisol",
  "Manufacturer": "Aristopharma",
  "OrigName": "chlorhexidine ",
  "Details": "Xisol is a preparation of chlorhexidine .\r\n\r\n  Clorhexidine is used as surgical hand scrub, healthcare personnel handwash, patient preoperative skin preparation and skin wound and general skin cleansing.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "STOMATOLOGICAL PREPARATIONS",
    "BLOOD SUBSTITUTES AND PERFUSION SOLUTIONS",
    "ANTISEPTICS AND DISINFECTANTS",
    "MEDICATED DRESSINGS",
    "THROAT PREPARATIONS",
    "OPHTHALMOLOGICALS",
    "OTOLOGICALS",
    "OPHTHALMOLOGICAL AND OTOLOGICAL PREPARATIONS"
  ]
}
{
  "Name": "Xitil",
  "Manufacturer": "Ziska",
  "OrigName": "cefuroxime",
  "Details": "Xitil is a preparation of cefuroxime.\n",
  "Dosage": "",
  "Available": [
    "Xitil 250 mg Tab:Contains cefuroxime 250 mg/tablet."
  ],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "Xmec",
  "Manufacturer": "Beacon",
  "OrigName": "meclizine + pyridoxine",
  "Details": "Xmec is a preparation of meclizine + pyridoxine.\n",
  "Dosage": "",
  "Available": [],
  "Classification": []
}
{
  "Name": "Xpa",
  "Manufacturer": "Aristopharma",
  "OrigName": "paracetamol/acetaminophen",
  "Details": "Xpa is a preparation of paracetamol/acetaminophen.\r\n\r\n  Paracetamol/acetaminophen is a pain reliever and fever reducer. It temporarily reduces fever\r\ntemporarily relieves minor aches and pains due to: headache, muscular aches, the common cold, minor pain of arthritis, backache, toothache, premenstrual and menstrual cramps.\n",
  "Dosage": "",
  "Available": [
    "Xpa 500 mg Tab.:Contains paracetamol/acetaminophen 500 mg/tablet.",
    "Xpa 120 mg/5 ml Susp.:Contains paracetamol/acetaminophen 120 mg/5 ml."
  ],
  "Classification": [
    "ANALGESICS"
  ]
}
{
  "Name": "Xpa-c",
  "Manufacturer": "Aristopharma",
  "OrigName": "paracetamol + caffeine",
  "Details": "Xpa-c is a preparation of paracetamol + caffeine.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANALGESICS"
  ]
}
{
  "Name": "Xpain",
  "Manufacturer": "Gaco",
  "OrigName": "aceclofenac",
  "Details": "Xpain is a preparation of aceclofenac.\r\n\r\n  Aceclofenac is a Nonsteroidal Anti-inflammatory Drug (NSAID) used in the management of osteoarthritis, rheumatoid arthritis, and ankylosing spondylitis.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS",
    "TOPICAL PRODUCTS FOR JOINT AND MUSCULAR PAIN"
  ]
}
{
  "Name": "Xpro",
  "Manufacturer": "Apex",
  "OrigName": "naproxen sodium",
  "Details": "Xpro is a preparation of naproxen sodium.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "OTHER GYNECOLOGICALS",
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS",
    "TOPICAL PRODUCTS FOR JOINT AND MUSCULAR PAIN"
  ]
}
{
  "Name": "Xripa",
  "Manufacturer": "Square",
  "OrigName": "nefopam",
  "Details": "Xripa is a preparation of nefopam.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANALGESICS"
  ]
}
{
  "Name": "Xtil",
  "Manufacturer": "Syntho",
  "OrigName": "cefuroxime",
  "Details": "Xtil is a preparation of cefuroxime.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "Xtrapel",
  "Manufacturer": "Beacon",
  "OrigName": "tramadol hydrochloride",
  "Details": "Xtrapel is a preparation of tramadol hydrochloride.\n",
  "Dosage": "",
  "Available": [
    "Xtrapel 100 Inj. :Contains Tramadol Hydrochloride 100 mg/2 ml.",
    "Xtrapel-SR 100 mg Capsule :Contains Tramadol Hydrochloride 100 mg SR."
  ],
  "Classification": [
    "ANALGESICS"
  ]
}
{
  "Name": "Xtrum Gold",
  "Manufacturer": "Globe",
  "OrigName": "multivitamin and multiminerals, A-Z gold",
  "Details": "Xtrum Gold is a preparation of multivitamin and multiminerals, A-Z gold.\n",
  "Dosage": "",
  "Available": [],
  "Classification": []
}
{
  "Name": "Xyfen",
  "Manufacturer": "Supreme",
  "OrigName": "aceclofenac",
  "Details": "Xyfen is a preparation of aceclofenac.\r\n\r\n  Aceclofenac is a Nonsteroidal Anti-inflammatory Drug (NSAID) used in the management of osteoarthritis, rheumatoid arthritis, and ankylosing spondylitis.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS",
    "TOPICAL PRODUCTS FOR JOINT AND MUSCULAR PAIN"
  ]
}
{
  "Name": "Xylib",
  "Manufacturer": "Libra",
  "OrigName": "ceftriaxone",
  "Details": "Xylib is a preparation of ceftriaxone.\n",
  "Dosage": "",
  "Available": [
    "Xylib 250 mg I.V. Inj.:Contains ceftriaxone 250 mg/vial. ml.",
    "Xylib 500 mg I.V. Inj.:Contains ceftriaxone 500 mg/vial. ml.",
    "Xylib 1 gm I.V. Inj.:Contains ceftriaxone 1 gm/vial.",
    "Xylib 250 mg I.M. Inj.:Contains ceftriaxone 250 mg/vial.",
    "Xylib 500 mg I.M. Inj.:Contains ceftriaxone 500 mg/vial."
  ],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "Xylo plus",
  "Manufacturer": "Incepta",
  "OrigName": "xylometazoline hcl + sodium chromoglycate",
  "Details": "Xylo plus is a preparation of xylometazoline hcl + sodium chromoglycate.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "Xylocaine",
  "Manufacturer": "Astra",
  "OrigName": "lidocaine HCl",
  "Details": "Xylocaine is a preparation of lidocaine HCl.\n",
  "Dosage": "",
  "Available": [
    "Xylocaine 2.5 % Cream :Contains lignocaine hydrochloride 2.5%."
  ],
  "Classification": [
    "CARDIAC THERAPY",
    "VASOPROTECTIVES",
    "ANTIPRURITICS, INCL. ANTIHISTAMINES, ANESTHETICS, ETC.",
    "ANESTHETICS",
    "THROAT PREPARATIONS",
    "OTOLOGICALS",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "xylometazoline",
  "Manufacturer": null,
  "OrigName": "elated Brand Name: Antazo",
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "NASAL PREPARATIONS",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "xylometazoline, combinations",
  "Manufacturer": null,
  "OrigName": "elated Brand Name: Xylo plu",
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "Xylone",
  "Manufacturer": "ACI",
  "OrigName": "lidocaine HCl",
  "Details": "Xylone is a preparation of lidocaine HCl.\n",
  "Dosage": "",
  "Available": [
    "Xylone 2% Inj.:Contains lignocaine hydrochloride 2%."
  ],
  "Classification": [
    "CARDIAC THERAPY",
    "VASOPROTECTIVES",
    "ANTIPRURITICS, INCL. ANTIHISTAMINES, ANESTHETICS, ETC.",
    "ANESTHETICS",
    "THROAT PREPARATIONS",
    "OTOLOGICALS",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "Xylovin",
  "Manufacturer": "Opso Saline",
  "OrigName": "xylometazoline",
  "Details": "Xylovin is a preparation of xylometazoline.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "NASAL PREPARATIONS",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "Xynofen",
  "Manufacturer": "Libra",
  "OrigName": "null",
  "Details": "Xynofen is a preparation of .\r\n\r\n  Cefixime is a third-generation cephalosporin antibacterial used to treat infections due to susceptible Gram-positive and Gram-negative bacteria, including gonorrhoea and infections of the respiratory and urinary tracts.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "Xynor",
  "Manufacturer": "Beximco",
  "OrigName": "ornidazole",
  "Details": "Xynor is a preparation of ornidazole.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "GYNECOLOGICAL ANTIINFECTIVES AND ANTISEPTICS",
    "ANTIBACTERIALS FOR SYSTEMIC USE",
    "ANTIPROTOZOALS"
  ]
}
{
  "Name": "Xytrex",
  "Manufacturer": "ACI",
  "OrigName": "olanzapine",
  "Details": "Xytrex is a preparation of olanzapine.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "PSYCHOLEPTICS"
  ]
}
