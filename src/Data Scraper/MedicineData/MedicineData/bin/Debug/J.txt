{
  "Name": "J-fenac",
  "Manufacturer": "Ad-din",
  "OrigName": "diclofenac sodium",
  "Details": "J-fenac is a preparation of diclofenac sodium.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "OTHER DERMATOLOGICAL PREPARATIONS",
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS"
  ]
}
{
  "Name": "J-mox",
  "Manufacturer": "Ad-din",
  "OrigName": "amoxycillin",
  "Details": "J-mox is a preparation of amoxycillin.\r\n\r\n  Amoxicillin is used in the treatment of infections due to susceptible (ONLY β-lactamase-negative) strains of the designated microorganisms in the conditions listed below:\n",
  "Dosage": "",
  "Available": [
    "J-mox250 mg Cap. :Contains amoxycillin 250 mg/capsule.",
    "J-mox 500 mg Cap. :Contains amoxycillin 500 mg/capsule.",
    "J-mox 125 mg/5 ml Susp. :Contains amoxycillin 125 mg/5 ml."
  ],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "J-Zinc",
  "Manufacturer": "Ad-din",
  "OrigName": "zinc sulfate",
  "Details": "J-Zinc is a preparation of zinc sulfate.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "Mineral Supplements"
  ]
}
{
  "Name": "Jaroxin",
  "Manufacturer": "Jayson",
  "OrigName": "levothyroxine",
  "Details": "Jaroxin is a preparation of levothyroxine.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "THYROID THERAPY"
  ]
}
{
  "Name": "Jasocaine",
  "Manufacturer": "Jayson",
  "OrigName": "lidocaine HCl",
  "Details": "Jasocaine is a preparation of lidocaine HCl.\n",
  "Dosage": "",
  "Available": [
    "Jasocaine 2% Jelly :Contains lignocaine hydrochloride 2%.",
    "Jasocaine 2% Gel.:Contains lignocaine hydrochloride 2%.",
    "Jasocaine 1% Inj.:Contains lignocaine hydrochloride 1%.",
    "Jasocaine 2% Inj.:Contains lignocaine hydrochloride 2%."
  ],
  "Classification": [
    "CARDIAC THERAPY",
    "VASOPROTECTIVES",
    "ANTIPRURITICS, INCL. ANTIHISTAMINES, ANESTHETICS, ETC.",
    "ANESTHETICS",
    "THROAT PREPARATIONS",
    "OTOLOGICALS",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "Jasocaine-A",
  "Manufacturer": "Jayson",
  "OrigName": "lidocaine HCl + adrenaline",
  "Details": "Jasocaine-A is a preparation of lidocaine HCl + adrenaline.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "CARDIAC THERAPY",
    "VASOPROTECTIVES",
    "ANTIPRURITICS, INCL. ANTIHISTAMINES, ANESTHETICS, ETC.",
    "ANESTHETICS",
    "THROAT PREPARATIONS",
    "OTOLOGICALS",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "Jasocal",
  "Manufacturer": "Jayson",
  "OrigName": "calcium carbonate",
  "Details": "Jasocal is a preparation of calcium carbonate.\r\n\r\n  Calcium carbonate is used as an antacid and as a calcium supplement in deficiency states and as an adjunct in the management of osteoporosis.\n",
  "Dosage": "",
  "Available": [
    "Jasocal 250 mg Tab.:Contains elemental calcium 250 mg/tablet (as carbonate)."
  ],
  "Classification": [
    "DRUGS FOR ACID RELATED DISORDERS",
    "Mineral Supplements"
  ]
}
{
  "Name": "Jasochlor",
  "Manufacturer": "Jayson",
  "OrigName": "chloroquine",
  "Details": "Jasochlor is a preparation of chloroquine.\r\n\r\n  Chloroquine is an antimalarial drug used in the treatment and prophylaxis of malaria and treatment of hepatic amoebiasis.\n",
  "Dosage": "",
  "Available": [
    "Jasochlor 250 mg Tab. :Contains chloroquine phosphate 250 mg/tablet."
  ],
  "Classification": [
    "ANTIPROTOZOALS"
  ]
}
{
  "Name": "Jasophylin",
  "Manufacturer": "Jayson",
  "OrigName": "theophylline sodium glycinate",
  "Details": "Jasophylin is a preparation of theophylline sodium glycinate.\n",
  "Dosage": "",
  "Available": [
    "Jasophylin 300 mg Tab. :Contains theophylline 300 mg/tablet."
  ],
  "Classification": [
    "DRUGS FOR OBSTRUCTIVE AIRWAY DISEASES"
  ]
}
{
  "Name": "Jasoprim",
  "Manufacturer": "Jayson",
  "OrigName": "primaquine phosphate",
  "Details": "Jasoprim is a preparation of primaquine phosphate.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIPROTOZOALS"
  ]
}
{
  "Name": "Jasoquin",
  "Manufacturer": "Jayson",
  "OrigName": "quinine",
  "Details": "Jasoquin is a preparation of quinine.\n",
  "Dosage": "",
  "Available": [
    "Jasoquin 300 mg Tab. :Contains quinine sulphate 300 mg/tablet."
  ],
  "Classification": [
    "ANTIPROTOZOALS"
  ]
}
{
  "Name": "Jasotrim",
  "Manufacturer": "Jayson",
  "OrigName": "sulfamethoxazole + trimethoprim",
  "Details": "Jasotrim is a preparation of sulfamethoxazole + trimethoprim.\n",
  "Dosage": "",
  "Available": [
    "Jasotrim 400/80 mg Tab.:Contains sulphamethoxazole 400 mg + trimethoprim 80 mg/tablet."
  ],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "Jasovit",
  "Manufacturer": "Jayson",
  "OrigName": "multivitamin, plain",
  "Details": "Jasovit is a preparation of multivitamin, plain.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "Vitamins"
  ]
}
{
  "Name": "Jasovit-M",
  "Manufacturer": "Jayson",
  "OrigName": "multivitamins and multiminerals",
  "Details": "Jasovit-M is a preparation of multivitamins and multiminerals.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "Vitamins"
  ]
}
{
  "Name": "Jedine",
  "Manufacturer": "Ad-din",
  "OrigName": "cefradine",
  "Details": "Jedine is a preparation of cefradine.\r\n\r\n  Cefradine, a semisynthetic cephalosporin antibiotic, is indicated for the treatment of patients with mild to moderate infections caused by susceptible strains of the designated microorganisms in the conditions listed below:\n",
  "Dosage": "",
  "Available": [
    "Jedine 500 mg Cap. :Contains cephradine 500 mg/capsule.",
    "Jedine 125 mg/5 ml Susp. :Contains cephradine 125 mg/5 ml."
  ],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "Jefril",
  "Manufacturer": "Jayson",
  "OrigName": "pseudoephedrine + guaiphenesin + triprolidine",
  "Details": "Jefril is a preparation of pseudoephedrine + guaiphenesin + triprolidine.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "NASAL PREPARATIONS"
  ]
}
{
  "Name": "Joinix",
  "Manufacturer": "Incepta",
  "OrigName": "glucosamine",
  "Details": "Joinix is a preparation of glucosamine.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS"
  ]
}
{
  "Name": "Joinix Plus",
  "Manufacturer": "Incepta",
  "OrigName": "glucosamine and chondroitin",
  "Details": "Joinix Plus is a preparation of glucosamine and chondroitin.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS"
  ]
}
{
  "Name": "Jointec Plus",
  "Manufacturer": "Beximco",
  "OrigName": "glucosamine and chondroitin",
  "Details": "Jointec Plus is a preparation of glucosamine and chondroitin.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS"
  ]
}
{
  "Name": "Jorix",
  "Manufacturer": "Apex",
  "OrigName": "glucosamine and chondroitin",
  "Details": "Jorix is a preparation of glucosamine and chondroitin.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS"
  ]
}
{
  "Name": "josamycin",
  "Manufacturer": null,
  "OrigName": null,
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "Jpdrox",
  "Manufacturer": "Jayson",
  "OrigName": "null",
  "Details": "Jpdrox is a preparation of .\r\n\r\n  This combination is used to treat gastric hyper-acidity.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "DRUGS FOR ACID RELATED DISORDERS"
  ]
}
{
  "Name": "Jpdrox-s",
  "Manufacturer": "Jayson",
  "OrigName": "null",
  "Details": "Jpdrox-s is a preparation of .\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "DRUGS FOR ACID RELATED DISORDERS"
  ]
}
{
  "Name": "Junivit",
  "Manufacturer": "Ibn Sina",
  "OrigName": "vitamin A, D, B1, B2, B6, C, E, nicotinamide & cod liver oil",
  "Details": "Junivit is a preparation of vitamin A, D, B1, B2, B6, C, E, nicotinamide & cod liver oil.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "Vitamins"
  ]
}
