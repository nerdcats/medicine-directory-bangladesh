﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicineDirectoryBD.Helpers
{
    public static class Constants
    {
        public static readonly string LoadingMedicineDetails = "Loading Medicine Details";
        public static readonly string NavigationToken = "Navigate";
        public static readonly string ShowToast = "ShowToast";
        public static  int MAXFAV = 15;
    }
}
