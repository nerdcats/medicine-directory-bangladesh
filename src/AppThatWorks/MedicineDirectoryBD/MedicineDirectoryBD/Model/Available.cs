﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicineDirectoryBD.Model
{
    [Table]
    public class Available : ObservableObject
    {
        private int _SerialId;

        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int SerialId
        {
            get { return _SerialId; }
            set
            {
                if (_SerialId == value)
                {
                    return;
                }
                _SerialId = value;
                RaisePropertyChanging("SerialId");  
                RaisePropertyChanged("SerialId");
            }
        }


        private string _name;

        [Column]
        public string Name
        {
            get { return _name; }
            set
            {
                if (_name == value)
                {
                    return;
                }
                _name = value;
                RaisePropertyChanging("Name");
                RaisePropertyChanged("Name");
            }
        }

        private int _medicineId;

        [Column]
        public int MedicineId
        {
            get { return _medicineId; }
            set
            {
                if (_medicineId == value)
                {
                    return;
                }
                _medicineId = value;
                RaisePropertyChanging("MedicineId");
                RaisePropertyChanged("MedicineId");
            }
        }
    }
}
