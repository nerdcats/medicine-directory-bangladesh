﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MedicineDirectoryBD.Resources;
using Microsoft.Phone.Reactive;
using Microsoft.Phone.Shell;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using MedicineDirectoryBD.Model;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using Coding4Fun.Toolkit.Controls;
using MedicineDirectoryBD.Helpers;
using GalaSoft.MvvmLight.Messaging;
using System.IO.IsolatedStorage;
using Microsoft.Phone.Tasks;

namespace MedicineDirectoryBD.ViewModel
{
    public class MedicineDbViewModel : ViewModelBase
    {
        private IsolatedStorageSettings _settings = IsolatedStorageSettings.ApplicationSettings;

        
        public const string SelectedPivotIndexPropertyName = "SelectedPivotIndex";

        private int _SelectedPivotIndex = 0;

        
        public int SelectedPivotIndex
        {
            get
            {
                return _SelectedPivotIndex;
            }

            set
            {
                if (_SelectedPivotIndex == value)
                {
                    return;
                }

                RaisePropertyChanging(SelectedPivotIndexPropertyName);
                _SelectedPivotIndex = value;
                RaisePropertyChanged(SelectedPivotIndexPropertyName);
            }
        }



        // const string ConnectionString = "Data Source=isostore:/MedicineDatabase.sdf";
        const string ConnectionString = "Data Source='appdata:/MedicineDatabase.sdf'; File Mode = read only;";

      

       private CancellationTokenSource _SearchCancellationToken;

        private readonly MedicineDbDataContext _medicineDbDataContext;



        
        public const string FavouritesListPropertyName = "FavouritesList";

        private ObservableCollection<Medicine> _FavouritesList = null;

        public ObservableCollection<Medicine> FavouritesList
        {
            get
            {
                return _FavouritesList;
            }

            set
            {
                if (_FavouritesList == value)
                {
                    return;
                }

                RaisePropertyChanging(FavouritesListPropertyName);
                _FavouritesList = value;
                RaisePropertyChanged(FavouritesListPropertyName);
            }
        }

        private ObservableCollection<Medicine> _medicines; 
        public ObservableCollection<Medicine> Medicines
        {
            get { return _medicines; }
            set
            {
                if (_medicines == value)
                {
                    return;
                }
                _medicines = value;
                RaisePropertyChanged("Medicines");
                RaisePropertyChanging("Medicines");
            }
        }

        private ObservableCollection<Classification> _classifications;
        public ObservableCollection<Classification> Classifications
        {
            get { return _classifications; }
            set
            {
                if (_classifications == value)
                {
                    return;
                }
                _classifications = value;
                RaisePropertyChanged("Classifications");
                RaisePropertyChanging("Classifications");
            }
        }

      
        public const string AvailableCollectionPropertyName = "AvailableCollection";

        private ObservableCollection<Available> _AvailableCollection = null;

      
        public ObservableCollection<Available> AvailableCollection
        {
            get
            {
                return _AvailableCollection;
            }

            set
            {
                if (_AvailableCollection == value)
                {
                    return;
                }

                RaisePropertyChanging(AvailableCollectionPropertyName);
                _AvailableCollection = value;
                RaisePropertyChanged(AvailableCollectionPropertyName);
            }
        }

        public const string IsFavEmptyPropertyName = "IsFavEmpty";

        private bool _IsFavEmpty = true;

        public bool IsFavEmpty
        {
            get
            {
                return _IsFavEmpty;
            }

            set
            {
                if (_IsFavEmpty == value)
                {
                    return;
                }

                RaisePropertyChanging(IsFavEmptyPropertyName);
                _IsFavEmpty = value;
                RaisePropertyChanged(IsFavEmptyPropertyName);
            }
        }

        public ICommand SearchCommand { get; set; }

        public const string AddToFavouritesPropertyName = "AddToFavourites";

        private RelayCommand<object> _AddToFavourites = null;

        public RelayCommand<object> AddToFavourites
        {
            get
            {
                return _AddToFavourites;
            }

            set
            {
                if (_AddToFavourites == value)
                {
                    return;
                }

                RaisePropertyChanging(AddToFavouritesPropertyName);
                _AddToFavourites = value;
                RaisePropertyChanged(AddToFavouritesPropertyName);
            }
        }

      
        public const string DeleteFavouritesPropertyName = "DeleteFavourites";

        private RelayCommand<object> _DeleteFavourites = null;

        
        public RelayCommand<object> DeleteFavourites
        {
            get
            {
                return _DeleteFavourites;
            }

            set
            {
                if (_DeleteFavourites == value)
                {
                    return;
                }

                RaisePropertyChanging(DeleteFavouritesPropertyName);
                _DeleteFavourites = value;
                RaisePropertyChanged(DeleteFavouritesPropertyName);
            }
        }

        
        public const string SelectFavouriteIndexPropertyName = "SelectFavouriteIndex";

        private RelayCommand _SelectFavouriteIndex = null;

       
        public RelayCommand SelectFavouriteIndex
        {
            get
            {
                return _SelectFavouriteIndex;
            }

            set
            {
                if (_SelectFavouriteIndex == value)
                {
                    return;
                }

                RaisePropertyChanging(SelectFavouriteIndexPropertyName);
                _SelectFavouriteIndex = value;
                RaisePropertyChanged(SelectFavouriteIndexPropertyName);
            }
        }

       
        public const string ShowRatingTaskPropertyName = "ShowRatingTask";

        private RelayCommand _ShowRatingTask = null;

        public RelayCommand ShowRatingTask
        {
            get
            {
                return _ShowRatingTask;
            }

            set
            {
                if (_ShowRatingTask == value)
                {
                    return;
                }

                RaisePropertyChanging(ShowRatingTaskPropertyName);
                _ShowRatingTask = value;
                RaisePropertyChanged(ShowRatingTaskPropertyName);
            }
        }

        public MedicineDbViewModel()
        {
            _medicineDbDataContext = new MedicineDbDataContext(ConnectionString);
            
            Medicines = new ObservableCollection<Medicine>();
            Classifications = new ObservableCollection<Classification>();
            AvailableCollection = new ObservableCollection<Available>();
            FavouritesList = new ObservableCollection<Medicine>();
            

            SearchCommand = new RelayCommand<string>(OnSearchButtonTap);
            ShowAboutCommand = new RelayCommand(ShowAboutAction);
            ShowDetailsCommand = new RelayCommand<object>(ShowDetailsAction);
            AddToFavourites = new RelayCommand<object>(AddToFavouritesAction);
            DeleteFavourites = new RelayCommand<object>(DeleteFavouritesAction);
            SelectFavouriteIndex = new RelayCommand(SelectFavouriteIndexAction);
            ShowRatingTask = new RelayCommand(ShowRatingTaskAction);
            
            
          //  CreateDatabase();

           // ReadJsonAndAddMedicines();

            LoadFavourites();

            
            LoadMedicinesFromDatabase();
            LoadClassificationsFromDatabase();
            LoadAvailableAsListFromDatabase();
        }

        private void ShowRatingTaskAction()
        {
            MarketplaceReviewTask marketplaceReviewTask = new MarketplaceReviewTask();

            marketplaceReviewTask.Show();
        }

        private void SelectFavouriteIndexAction()
        {
            SelectedPivotIndex = 2;
        }

        public void DeleteFavouritesAction(object obj)
        {
            try
            {
                var medToDelete = FavouritesList.Where(x => x.MedicineId == (obj as Medicine).MedicineId).First();
                FavouritesList.Remove(medToDelete);
                SaveFavourites();

                if (FavouritesList.Count == 0)
                    IsFavEmpty = true;
            }
            catch
            { }
        }

        public void AddToFavouritesAction(object obj)
        {
            if (!FavouritesList.Any(x=>x.MedicineId.Equals((obj as Medicine).MedicineId)))
            {
                if (FavouritesList.Count >= Constants.MAXFAV)
                    FavouritesList.RemoveAt(0);

                if (IsFavEmpty)
                    IsFavEmpty = false;

                FavouritesList.Add(obj as Medicine);
                SaveFavourites();

                ToastPrompt toast = new ToastPrompt();
                toast.Title = "Medicine Directory BD";
                toast.Message = (obj as Medicine).Name+ "is added to the favourites";
                toast.Show();
            }
            else
                MessageBox.Show("This one is already on the favourites :)");
        }

        

        private void LoadFavourites()
        {
            if (_settings.Contains("Favourites"))
            {
                FavouritesList = _settings["Favourites"] as ObservableCollection<Medicine>;
                if (FavouritesList.Count == 0)
                    IsFavEmpty = true;
                else
                    IsFavEmpty = false;
            }
        }

        private void SaveFavourites()
        {
            if (_settings.Contains("Favourites"))
            {
                _settings["Favourites"] = FavouritesList;
            }
            else
            {
                _settings.Add("Favourites", FavouritesList);
            }
            _settings.Save();
        }

        private void ShowDetailsAction(object obj)
        {
            //notify the page to navigate and start progressbar


            var medicine = obj as Medicine;
            Uri uri = new Uri("/DetailsPage.xaml?selectedItem=" + (medicine.MedicineId - 1), UriKind.Relative);
            Messenger.Default.Send<Uri>(uri, Constants.NavigationToken);

            //I'm gonna change this, lot of refactoring needed. 
            
        }

       


        private void LoadAvailableAsListFromDatabase()
        {
            var availableAsinDb = from Available avail in _medicineDbDataContext.Availables
                                  select avail;

            AvailableCollection = new ObservableCollection<Available>(availableAsinDb);
        }

        


        private RelayCommand _ShowAboutCommand = null;
        public RelayCommand ShowAboutCommand { get { return _ShowAboutCommand; } set { _ShowAboutCommand = value; RaisePropertyChanged("ShowAboutCommand"); } }

       
        public const string ShowDetailsCommandPropertyName = "ShowDetailsCommand";

        private RelayCommand<object> _ShowDetailsCommand = null;

        
        public RelayCommand<object> ShowDetailsCommand
        {
            get
            {
                return _ShowDetailsCommand;
            }

            set
            {
                if (_ShowDetailsCommand == value)
                {
                    return;
                }

                RaisePropertyChanging(ShowDetailsCommandPropertyName);
                _ShowDetailsCommand = value;
                RaisePropertyChanged(ShowDetailsCommandPropertyName);
            }
        }



        private void ShowAboutAction()
        {
            AboutPrompt About = new AboutPrompt();
            About.Title = "About";
            About.Body = "All data is collected through independent\nsurveys of NerdCats. Let us know if you like it\non www.facebook.com/nerdcats";

            About.Show();
        }

        private void LoadClassificationsFromDatabase()
        {
            var classificationsInDb = from Classification classification
                                      in _medicineDbDataContext.Classifications
                                      select classification;

            Classifications = new ObservableCollection<Classification>(classificationsInDb);
        }

       

        private void OnSearchButtonTap(string param)
        {
            if (_SearchCancellationToken != null && !_SearchCancellationToken.IsCancellationRequested)
            {
                _SearchCancellationToken.Cancel();

                InvokeSearch(param);

            }
            else if (_SearchCancellationToken == null )
            {
                InvokeSearch(param);
            }

            
            

           // Medicines = new ObservableCollection<Medicine>(searchedMedicine.Result);
        }

        private void InvokeSearch(string param)
        {
            _SearchCancellationToken = new CancellationTokenSource();
            try
            {
                SearchMedicines(param, _SearchCancellationToken.Token, (x) => { Medicines = new ObservableCollection<Medicine>(x); });
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Search Cancelled");
            }
        }

        private async void SearchMedicines(string param , CancellationToken cts, Action<IQueryable<Medicine>> callback)
        {
            try
            {
                var searchedMedicines = await Task.Run<IQueryable<Medicine>>(() =>
                {


                    var searchedMedicine = from Medicine medicine
                                       in _medicineDbDataContext.Medicines
                                           where (medicine.Name.StartsWith(param) || medicine.OrigName.StartsWith(param) )
                                           select medicine;
                    try
                    {
                        cts.ThrowIfCancellationRequested();
                        
                    }
                    catch { }



                    return searchedMedicine;
                }, cts);
                callback(searchedMedicines);
            }
            catch { Debug.WriteLine("Task Cancelled"); }
           
        }

        private void ReadJsonAndAddMedicines()
        {
            using (var file = File.OpenText(@"Medicines.json"))
            {
                using (var reader = new JsonTextReader(file))
                {
                    var jArray = JArray.Load(reader);
                    int incrementer = 1;

                    foreach (var medicineObject in jArray.Select(medicine => medicine.ToObject<Medicine>()))
                    {
                        Medicines.Add(medicineObject);
                        foreach (var classification in medicineObject.Classification)
                        {
                            var classificationObj = new Classification {Name = classification, MedicineId = incrementer};
                            Classifications.Add(classificationObj);
                        }

                        foreach (var availableIn in medicineObject.Available)
                        {
                            var AvailableAsObj = new Available() {MedicineId=incrementer, Name=availableIn };
                            AvailableCollection.Add(AvailableAsObj);
                        }
                        incrementer = incrementer + 1;
                    }
                }
                AddMedicines();
                AddClassifications();
                AddAvailables();
            }
        }

        private void AddAvailables()
        {
            try
            {

                _medicineDbDataContext.Availables.InsertAllOnSubmit(AvailableCollection);
                _medicineDbDataContext.SubmitChanges();                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //private void CreateDatabase()
        //{
        //    using (var medicineDb = new MedicineDbDataContext(ConnectionString))
        //    {
        //        if (medicineDb.DatabaseExists() == false)
        //        {
        //            medicineDb.CreateDatabase();
        //            medicineDb.SubmitChanges();
        //        }   
        //    }
        //}

        public void SaveChangesToDB()
        {
            _medicineDbDataContext.SubmitChanges();
        }

        public void LoadMedicinesFromDatabase()
        {
            var medicinesInDb = from Medicine medicine 
                                in _medicineDbDataContext.Medicines
                                select medicine;

            Medicines = new ObservableCollection<Medicine>(medicinesInDb);
        }

        public void AddMedicines()
        {
            try
            {
                _medicineDbDataContext.Medicines.InsertAllOnSubmit(Medicines);
                _medicineDbDataContext.SubmitChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void AddClassifications()
        {
            try
            {
                _medicineDbDataContext.Classifications.InsertAllOnSubmit(Classifications);
                _medicineDbDataContext.SubmitChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public IList<Medicine> GetSimiliarMedicines(Medicine medicine)
        {
            try
            {
                var SameTypeOfMedicines = from Medicine med in _medicineDbDataContext.Medicines
                                          where med.OrigName != null && (med.OrigName.Equals(medicine.OrigName) || med.OrigName.Contains(medicine.OrigName)) && med.Name!=medicine.Name
                                          select med;

                return SameTypeOfMedicines.ToList();
            }
            catch
            {
                return null;
            }
        }

        public IList<Available> GetAvailableAsList(Medicine medicine)
        {
            try
            {
                var availableAs = from a in AvailableCollection
                                  where medicine.MedicineId.Equals(a.MedicineId)
                                  select a;
                
                return availableAs.ToList();
            }
            catch
            {
                return null; 
            }
        }

        public IList<Classification> GetClassification(Medicine medicine)
        {
            var classification = from c in Classifications
                                 where c.MedicineId.Equals(medicine.MedicineId)
                                 select c;
            classification= classification.Distinct();

            return classification.ToList();
        }

        public IList<Medicine> GetRelatedMedicines(Medicine medicine)
        {
            var commonClassification = (from c in Classifications
                where c.MedicineId == medicine.MedicineId
                select c).FirstOrDefault();

            var medicineWithSimilarClassification = Classifications.Where(classification => commonClassification != null && classification.Name.Contains(commonClassification.Name)).ToList();

            var relatedMedicines = from c in medicineWithSimilarClassification
                                   join m in _medicineDbDataContext.Medicines on c.MedicineId equals m.MedicineId
                select m;

            return relatedMedicines.ToList();

        }
    }
}
